FROM python:3.7

COPY ./common /common
COPY ./aggregation /aggregation
COPY ./requirements.txt /aggregation
WORKDIR /aggregation
RUN pip install -r requirements.txt

CMD ["python", "main.py"]