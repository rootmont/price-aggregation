from flask import Flask
from flask_api import status
from flask import Response
from apscheduler.schedulers.background import BackgroundScheduler
import datetime

app = Flask(__name__)

@app.route('/')
def root():
    return 'I am a teapot.', 420


@app.route('/health',  methods=['GET'])
def health():
    if not scheduler.running or scheduler.get_jobs().__len__() == 0:
        return Response("{}", status=status.HTTP_500_INTERNAL_SERVER_ERROR, mimetype='application/json')
    return Response("{}", status=status.HTTP_200_OK, mimetype='application/json')



now = datetime.datetime.now()
if __name__ == '__main__':
    scheduler = BackgroundScheduler()
    scheduler.add_job(lambda: print('give me code'), trigger='interval', seconds=60, max_instances=1)
