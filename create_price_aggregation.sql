CREATE TABLE `price_aggregation` (
  `coin_id` int(15) NOT NULL,
  `my_date` date NOT NULL,
  `price_high` float(30,15) unsigned DEFAULT '0.000000000000000',
  `price_low` float(30,15) unsigned DEFAULT '0.000000000000000',
  `price_open` float(30,15) unsigned DEFAULT '0.000000000000000',
  `price_close` float(30,15) unsigned DEFAULT '0.000000000000000',
  `trading_volume` bigint(20) unsigned DEFAULT '0',
  `marketcap` bigint(20) unsigned DEFAULT '0',
  PRIMARY KEY (`coin_id`,`my_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
